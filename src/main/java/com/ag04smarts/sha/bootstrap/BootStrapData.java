package com.ag04smarts.sha.bootstrap;

import com.ag04smarts.sha.domain.Gender;
import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.domain.Status;
import com.ag04smarts.sha.repositories.PatientRepository;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;

@Component
public class BootStrapData {

    private final PatientRepository patientRepository;

    public BootStrapData(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @PostConstruct
    public void initData() {
        System.out.println("Loading Patient Data");

        Patient p1 = new Patient();
        p1.setFirstName("Liam");
        p1.setLastName("Payne");
        p1.setAge(21);
        p1.setPhoneNumber("324");

        patientRepository.save(p1);

        p1.setEmail("payne@gmail.com");
        p1.setGender(Gender.MALE);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 11, 31);
        Date date = calendar.getTime();

        p1.setEnlistmentDate(date);
        p1.setStatus(Status.ENLISTED);

        patientRepository.save(p1);

        System.out.println("Patients saved: " + patientRepository.count());

    }
}
