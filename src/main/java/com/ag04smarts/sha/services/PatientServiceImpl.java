package com.ag04smarts.sha.services;

import com.ag04smarts.sha.converters.EnlistmentFormToPatient;
import com.ag04smarts.sha.domain.EnlistmentForm;
import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.repositories.PatientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PatientServiceImpl implements PatientService{

    private final PatientRepository patientRepository;
    private final EnlistmentFormToPatient enlistmentFormToPatient;

    public PatientServiceImpl(PatientRepository patientRepository, EnlistmentFormToPatient enlistmentFormToPatient) {
        this.patientRepository = patientRepository;
        this.enlistmentFormToPatient = enlistmentFormToPatient;
    }

    @Override
    public List<Patient> findAllPatients() {
        return (List<Patient>) patientRepository.findAll();
    }

    @Override
    public Patient findPatientById(Long id) {

        Optional<Patient> patientOptional = patientRepository.findById(id);
        return patientOptional.get();
    }

    @Override
    public Patient savePatient(Patient patient) {
        return patientRepository.saveAndFlush(patient);
    }

    @Override
    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }

    @Override
    public Patient updatePatient(Long id, Patient patient) {
        Patient patient2 = patientRepository.findById(id).get();

        patient2.setLastName(patient.getLastName());
        patient2.setFirstName(patient.getFirstName());
        patient2.setAge(patient.getAge());
        patient2.setPhoneNumber(patient.getPhoneNumber());

        return patientRepository.saveAndFlush(patient2);
    }

    @Override
    public String getGreeting() {
        log.info("I am in the greeting method!");
        return "Hello, welcome to the Neurology Department!";
    }

    @Override
    public Patient saveEnlistmentForm(EnlistmentForm enlistmentForm) {
        Patient patient = enlistmentFormToPatient.convert(enlistmentForm);

        return patientRepository.saveAndFlush(patient);
    }

    @Transactional
    @Override
    public void savePictureFile(Long patientId, MultipartFile file) {

        try {
            Patient patient = patientRepository.findById(patientId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;

            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }

            patient.setPicture(byteObjects);

            patientRepository.save(patient);
        } catch (IOException e) {
            log.error("Error occurred", e);

            e.printStackTrace();
        }
    }

}
