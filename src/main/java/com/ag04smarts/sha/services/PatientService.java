package com.ag04smarts.sha.services;

import com.ag04smarts.sha.domain.EnlistmentForm;
import com.ag04smarts.sha.domain.Patient;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PatientService {

    Patient findPatientById(Long id);

    List<Patient> findAllPatients();

    Patient savePatient(Patient patient);

    void deletePatient(Long id);

    Patient updatePatient(Long id, Patient patient);

    String getGreeting();

    Patient saveEnlistmentForm(EnlistmentForm enlistmentForm);

    void savePictureFile(Long patientId, MultipartFile file);
}
