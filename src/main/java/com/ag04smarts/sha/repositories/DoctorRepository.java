package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.Doctor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DoctorRepository extends CrudRepository<Doctor, Long> {}
