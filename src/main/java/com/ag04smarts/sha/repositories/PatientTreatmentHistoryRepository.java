package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.PatientTreatmentHistory;
import org.springframework.data.repository.CrudRepository;

public interface PatientTreatmentHistoryRepository extends CrudRepository<PatientTreatmentHistory, Long> {
}
