package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.domain.Symptom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Long> {

    @Query("FROM Patient p WHERE p.age > 21 AND p.enlistmentDate > '2020-01-01'")
    public List<Patient> findByAgeAndEnlistmentDate();

    @Query("FROM Patient p " +
            "JOIN PatientMedicalRecord pmr          ON p.id = pmr.patient " +
            "JOIN pmr.symptoms s                    WHERE s.description = 'Fever' OR s.description = 'Coughing'")
    public List<Patient> findByDiagnosis();

}
