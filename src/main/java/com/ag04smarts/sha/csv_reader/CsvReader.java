package com.ag04smarts.sha.csv_reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;


public class CsvReader {
    private static final String SAMPLE_CSV_FILE_PATH = "src/main/resources/patients.csv";

    public static void read() throws IOException {
        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withIgnoreHeaderCase()
                        .withTrim());
        ) {
            for (CSVRecord csvRecord : csvParser) {
                String name = csvRecord.get("FirstName");
                String email = csvRecord.get("LastName");
                String phone = csvRecord.get("Email");
                String country = csvRecord.get("Age");

                System.out.println("Record No - " + csvRecord.getRecordNumber());
                System.out.println("---------------");
                System.out.println("FirstName : " + name);
                System.out.println("LastName : " + email);
                System.out.println("Email : " + phone);
                System.out.println("Age : " + country);
                System.out.println("---------------\n\n");
            }
        }
    }

}