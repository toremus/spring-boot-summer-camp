package com.ag04smarts.sha.controllers;

import com.ag04smarts.sha.converters.AppointmentFormToAppointment;
import com.ag04smarts.sha.converters.AppointmentToAppointmentDto;
import com.ag04smarts.sha.domain.Appointment;
import com.ag04smarts.sha.domain.AppointmentDto;
import com.ag04smarts.sha.domain.AppointmentForm;
import com.ag04smarts.sha.repositories.AppointmentRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Api(description = "This is my Appointment Controller")
@RestController
@RequestMapping(AppointmentController.BASE_URL)
public class AppointmentController {

    public static final String BASE_URL = "api/appointment";

    private final AppointmentFormToAppointment appointmentFormToAppointment;
    private final AppointmentRepository appointmentRepository;
    private final AppointmentToAppointmentDto appointmentToAppointmentDto;

    public AppointmentController(AppointmentFormToAppointment appointmentFormToAppointment, AppointmentRepository appointmentRepository, AppointmentToAppointmentDto appointmentToAppointmentDto) {
        this.appointmentFormToAppointment = appointmentFormToAppointment;
        this.appointmentRepository = appointmentRepository;
        this.appointmentToAppointmentDto = appointmentToAppointmentDto;
    }

    @ApiOperation(value = "Create a new Appointment")
    @PostMapping
    public @NonNull ResponseEntity<AppointmentDto> setAppointment(@RequestBody AppointmentForm appointmentForm) {
        Appointment appointment = appointmentFormToAppointment.convert(appointmentForm);

        Optional<Appointment> appointmentOptional = appointmentRepository.findByPatientAndDoctorAndAppointmentDate(appointment.getPatient(), appointment.getDoctor(), appointment.getAppointmentDate());
        if (appointmentOptional.isEmpty()) {
            Appointment appointment1 = appointmentRepository.save(appointment);
            AppointmentDto appointmentDto = appointmentToAppointmentDto.convert(appointment1);
            return new ResponseEntity<AppointmentDto>(appointmentDto, HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<AppointmentDto>(HttpStatus.BAD_REQUEST);
        }

    }
}
