package com.ag04smarts.sha.controllers;

import com.ag04smarts.sha.domain.EnlistmentForm;
import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.services.PatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.util.Set;

@Api(description = "This is my Enlistment Controller")
@Slf4j
@RestController
@RequestMapping(EnlistmentController.BASE_URL)
public class EnlistmentController {

    public static final String BASE_URL = "api/enlistment";

    private final PatientService patientService;
    private final MessageSource messageSource;
    private final Validator validator;

    public EnlistmentController(PatientService patientService, MessageSource messageSource, Validator validator) {
        this.patientService = patientService;
        this.messageSource = messageSource;
        this.validator = validator;
    }

    @ApiOperation(value = "Enlist a new Patient")
    @PostMapping
    public ResponseEntity<Patient> setEnlistment(@Valid @RequestBody EnlistmentForm enlistmentForm, BindingResult bindingResult){

        Set<ConstraintViolation<EnlistmentForm>> violations = validator.validate(enlistmentForm);

        if (violations.isEmpty()) {
            Patient patient = patientService.saveEnlistmentForm(enlistmentForm);

            return new ResponseEntity<Patient>(patient, HttpStatus.CREATED);
        }

        else {
            StringBuilder message = new StringBuilder();
            for (ConstraintViolation<EnlistmentForm> violation : violations) {
                message.append(violation.getMessage().concat("\n"));
            }
            System.out.println(message);
        }

        return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);

    }
}
