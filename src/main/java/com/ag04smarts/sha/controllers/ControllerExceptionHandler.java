package com.ag04smarts.sha.controllers;

import io.swagger.annotations.Api;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.NoSuchElementException;


@Api(description = "This is a Controller for handling exceptions")
@ControllerAdvice
public class ControllerExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataAccessException.class)
    public void handleNotFoundExceptions(DataAccessException exception) {
        System.out.println("Data access error!");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NoSuchElementException.class)
    public void handleNotFoundExceptions(NoSuchElementException exception) {
        System.out.println("Not found, sorry!");
    }

    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @ExceptionHandler(Exception.class)
    public void handleAllExceptions(Exception exception) {
        System.out.println("An error has occurred! We are so so sorry.");
    }
}
