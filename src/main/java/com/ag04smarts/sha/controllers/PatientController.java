package com.ag04smarts.sha.controllers;

import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.repositories.PatientRepository;
import com.ag04smarts.sha.services.PatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.List;

@Api(description = "This is my Patient Controller")
@Slf4j
@RestController
@RequestMapping(PatientController.BASE_URL)
@Transactional
public class PatientController {

    public static final String BASE_URL = "api/patient";

    private final PatientService patientService;
    private final PatientRepository patientRepository;


    public PatientController(PatientService patientService, PatientRepository patientRepository) {
        this.patientService = patientService;
        this.patientRepository = patientRepository;
    }

    public void queryMethod() {

        log.info("I am in the query method!");

        List<Patient> patientList = patientRepository.findByAgeAndEnlistmentDate();

        List<Patient> patientList2 = patientRepository.findByDiagnosis();

        System.out.println(patientList);
        System.out.println(patientList2);
    }

    @ApiOperation(value = "View a list of all Patients")
    @GetMapping
    List<Patient> getAllPatients(){
        return patientService.findAllPatients();
    }

    @ApiOperation(value = "Get a Patient by id")
    @GetMapping("/{id}")
    public ResponseEntity<Patient> getPatientById(@PathVariable Long id) {

        Patient patient = patientService.findPatientById(id);

        new ResponseEntity<Patient>(HttpStatus.OK);
        return ResponseEntity.ok().body(patient);
    }

    @ApiOperation(value = "Create a new Patient")
    @PostMapping
    public ResponseEntity<Patient> setPatient(@RequestBody Patient patient){

        patientService.savePatient(patient);

        new ResponseEntity<Patient>(HttpStatus.CREATED);
        return ResponseEntity.created(URI.create("http://localhost:8080/api/patient")).body(patient);
    }

    @ApiOperation(value = "Delete a Patient")
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deletePatient(@PathVariable Long id){
        
        Patient patient = patientService.findPatientById(id);

        new ResponseEntity<Patient>(HttpStatus.OK);
        return ResponseEntity.ok().body(id);
    }

    @ApiOperation(value = "Update a Patient", notes = "Update all or several properties")
    @PutMapping("/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable Long id, @RequestBody Patient patient){
        patientService.updatePatient(id, patient);

        new ResponseEntity<Patient>(HttpStatus.OK);
        return ResponseEntity.ok().body(patient);
    }

    @ApiOperation(value = "Update a Patient image")
    @PostMapping("picture/{id}")
    public ResponseEntity<Long> handleImagePost(@PathVariable Long id, @RequestParam("file") MultipartFile file){
        patientService.savePictureFile(id, file);

        new ResponseEntity<Patient>(HttpStatus.OK);
        return ResponseEntity.ok().body(id);
    }

    public String sayHello() {
        return patientService.getGreeting();
    }
}
