package com.ag04smarts.sha.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AppointmentForm {

    private Long patientId;
    private Long doctorId;
    private Date appointmentDate;

}
