package com.ag04smarts.sha.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@EqualsAndHashCode(exclude = {"patient", "doctor"})
@ToString(exclude = {"patient", "doctor"})
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Patient patient;

    @JsonIgnore
    @ManyToOne
    private Doctor doctor;

    private Date appointmentDate;

    public Appointment() {
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Appointment;
    }

}
