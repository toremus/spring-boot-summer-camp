package com.ag04smarts.sha.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(callSuper=true)
public class Doctor extends Person{

    @Enumerated(EnumType.STRING)
    private DoctorExpertise doctorExpertise;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctor", orphanRemoval = true)
    private Set<Appointment> appointments = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctor", orphanRemoval = true)
    private Set<PatientTreatmentHistory> patientTreatmentHistories = new HashSet<>();


}
