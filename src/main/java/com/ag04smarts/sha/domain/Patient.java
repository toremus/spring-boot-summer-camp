package com.ag04smarts.sha.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(callSuper=true)
@ToString(exclude = {"patientTreatmentHistories", "appointments", "picture"})
public class Patient extends Person{

    private String email;
    private Integer age;
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private Date enlistmentDate;

    @Enumerated(EnumType.STRING)
    private Status status;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", orphanRemoval = true)
    private PatientMedicalRecord patientMedicalRecord;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", orphanRemoval = true)
    private Set<PatientTreatmentHistory> patientTreatmentHistories = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", orphanRemoval = true)
    private Set<Appointment> appointments = new HashSet<>();

    @Lob
    private Byte[] picture;

    @PrePersist
    private void prePersistFunction() {
        System.out.println("Adding a new patient!");
    }

    @PreUpdate
    public void preUpdateFunction(){
        System.out.println("Updating a patient!");
    }

}
