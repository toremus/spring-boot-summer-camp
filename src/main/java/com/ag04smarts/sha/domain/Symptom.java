package com.ag04smarts.sha.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = {"patientMedicalRecords"})
@ToString(exclude = {"patientMedicalRecords"})
public class Symptom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String description;

    @ManyToMany(mappedBy = "symptoms")
    private Set<PatientMedicalRecord> patientMedicalRecords = new HashSet<>();

}
