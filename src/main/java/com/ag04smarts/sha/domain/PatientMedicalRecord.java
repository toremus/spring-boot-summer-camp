package com.ag04smarts.sha.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = {"patient", "symptoms"})
@ToString(exclude = {"patient", "symptoms"})
public class PatientMedicalRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Patient patient;

    private String diagnosis;
    private String treatment;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "patientMedicalRecord_symptom",
        joinColumns = @JoinColumn(name = "patientMedicalRecord_id"),
        inverseJoinColumns = @JoinColumn(name = "symptom_id"))
    private Set<Symptom> symptoms = new HashSet<>();

    public void addSymptom(Symptom symptom) {
        this.symptoms.add(symptom);
        symptom.getPatientMedicalRecords().add(this);
    }

    public void removeSymptom(Symptom symptom) {
        this.symptoms.remove(symptom);
        symptom.getPatientMedicalRecords().remove(this);
    }

}
