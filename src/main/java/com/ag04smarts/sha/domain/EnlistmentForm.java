package com.ag04smarts.sha.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class EnlistmentForm {

    @Valid
    @NotBlank(message = "{not.blank.first.name}")
    private String firstName;

    @Valid
    @NotBlank(message = "{not.blank.last.name}")
    private String lastName;

    @Valid
    @Email(message = "{email}")
    private String email;

    @Valid
    @Min(5)
    private Integer age;

    private String phoneNumber;
    private Gender gender;
    private Date enlistmentDate;
    private Status status;

}
