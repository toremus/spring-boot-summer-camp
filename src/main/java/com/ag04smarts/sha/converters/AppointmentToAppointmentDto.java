package com.ag04smarts.sha.converters;

import com.ag04smarts.sha.domain.Appointment;
import com.ag04smarts.sha.domain.AppointmentDto;
import com.ag04smarts.sha.domain.Doctor;
import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.repositories.AppointmentRepository;
import com.ag04smarts.sha.repositories.DoctorRepository;
import com.ag04smarts.sha.repositories.PatientRepository;
import lombok.NonNull;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AppointmentToAppointmentDto implements Converter<Appointment, AppointmentDto> {

    private AppointmentRepository appointmentRepository;
    private PatientRepository patientRepository;
    private DoctorRepository doctorRepository;

    public AppointmentToAppointmentDto(AppointmentRepository appointmentRepository, PatientRepository patientRepository, DoctorRepository doctorRepository) {
        this.appointmentRepository = appointmentRepository;
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
    }

    @Synchronized
    @NonNull
    @Override
    public AppointmentDto convert(Appointment source) {
        if (source == null) {
            return null;
        }

        AppointmentDto appointmentDto = new AppointmentDto();

        Patient patient = source.getPatient();
        Doctor doctor = source.getDoctor();

        appointmentDto.setPatientId(patient.getId());
        appointmentDto.setDoctorId(doctor.getId());
        appointmentDto.setAppointmentDate(source.getAppointmentDate());

        return appointmentDto;
    }
}
