package com.ag04smarts.sha.converters;

import com.ag04smarts.sha.domain.EnlistmentForm;
import com.ag04smarts.sha.domain.Patient;
import lombok.NonNull;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EnlistmentFormToPatient implements Converter<EnlistmentForm, Patient> {

    @Synchronized
    @NonNull
    @Override
    public Patient convert(EnlistmentForm source) {

        if (source == null) {
            return null;
        }

        Patient patient = new Patient() ;

        patient.setFirstName(source.getFirstName());
        patient.setLastName(source.getLastName());
        patient.setEmail(source.getEmail());
        patient.setAge(source.getAge());
        patient.setPhoneNumber(source.getPhoneNumber());
        patient.setGender(source.getGender());
        patient.setEnlistmentDate(source.getEnlistmentDate());
        patient.setStatus(source.getStatus());

        return patient;
    }
}
