package com.ag04smarts.sha.converters;

import com.ag04smarts.sha.domain.Appointment;
import com.ag04smarts.sha.domain.AppointmentForm;
import com.ag04smarts.sha.domain.Doctor;
import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.repositories.AppointmentRepository;
import com.ag04smarts.sha.repositories.DoctorRepository;
import com.ag04smarts.sha.repositories.PatientRepository;
import lombok.NonNull;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AppointmentFormToAppointment implements Converter<AppointmentForm, Appointment> {

    private AppointmentRepository appointmentRepository;
    private PatientRepository patientRepository;
    private DoctorRepository doctorRepository;

    public AppointmentFormToAppointment(AppointmentRepository appointmentRepository, PatientRepository patientRepository, DoctorRepository doctorRepository) {
        this.appointmentRepository = appointmentRepository;
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
    }

    @Synchronized
    @NonNull
    @Override
    public Appointment convert(AppointmentForm source) {
        if (source == null) {
            return null;
        }

        Patient patient = patientRepository.findById(source.getPatientId()).get();
        Doctor doctor = doctorRepository.findById(source.getDoctorId()).get();

        Appointment appointment = new Appointment();
        appointment.setPatient(patient);
        appointment.setDoctor(doctor);
        appointment.setAppointmentDate(source.getAppointmentDate());

        return appointment;

    }
}


