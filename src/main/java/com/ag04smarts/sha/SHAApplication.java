package com.ag04smarts.sha;

import com.ag04smarts.sha.controllers.PatientController;
import com.ag04smarts.sha.csv_reader.CsvReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;


@SpringBootApplication
public class SHAApplication {

	public static void main(String[] args) throws IOException {

		ConfigurableApplicationContext ctx = SpringApplication.run(SHAApplication.class, args);

		PatientController patientController = (PatientController) ctx.getBean("patientController");

		System.out.println(patientController.sayHello());
		patientController.queryMethod();

		CsvReader.read();

	}
}

