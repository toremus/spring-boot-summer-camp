insert into doctor (first_name, last_name, doctor_expertise) values ('Holger', 'Palmgren', 'GENERAL_DOCTOR');
insert into doctor (first_name, last_name, doctor_expertise) values ('Peter', 'Teleborian', 'PEDIATRICIAN');
insert into doctor (first_name, last_name, doctor_expertise) values ('Harriet', 'Vanger', 'PLASTIC_SURGEON');

insert into patient (first_name, last_name, email, age, phone_number, gender, enlistment_date, status) values ('Annika', 'Giannini', 'agiannini@gmail.com', 43, '0945773827', 'FEMALE', '2020-10-20', 'DIAGNOSED');
insert into patient (first_name, last_name, email, age, phone_number, gender, enlistment_date, status) values ('Mikael', 'Blomkvist', 'blomkvist@gmail.com', 45, '092048873', 'MALE', '2020-04-12', 'UNDER_DIAGNOSIS');
insert into patient (first_name, last_name, email, age, phone_number, gender, enlistment_date, status) values ('Camilla', 'Salander', 'camilla@gmail.com', 24, '0919372884', 'FEMALE', '2019-04-25', 'CURED');

insert into patient_medical_record (patient_id, diagnosis, treatment) values (2, 'Allergies', 'Aerious');
insert into patient_medical_record (patient_id, diagnosis, treatment) values (1, 'Migraine', 'Maxalt');
insert into patient_medical_record (patient_id, diagnosis, treatment) values (3, 'Insomnia', 'Praxiten');

insert into symptom (description) values ('Fever');
insert into symptom (description) values ('Coughing');
insert into symptom (description) values ('Headache');
insert into symptom (description) values ('Fatigue');
insert into symptom (description) values ('Rhinorrhea');
insert into symptom (description) values ('Abdominal pain');

insert into patient_treatment_history (patient_id, doctor_id, treatment_remark, old_status, new_status) values (3, 1, 'Prescribed medication', 'UNDER_DIAGNOSIS', 'DIAGNOSED');
insert into patient_treatment_history (patient_id, doctor_id, treatment_remark, old_status, new_status) values (3, 1, 'Taking medication for 3 weeks', 'UNDER_TREATMENT', 'CURED');
insert into patient_treatment_history (patient_id, doctor_id, treatment_remark, old_status, new_status) values (2, 2, 'Taking medication', 'DIAGNOSED', 'UNDER_TREATMENT');
insert into patient_treatment_history (patient_id, doctor_id, treatment_remark, old_status, new_status) values (2, 2, 'Second round of medication', 'UNDER_TREATMENT', 'UNDER_TREATMENT');
insert into patient_treatment_history (patient_id, doctor_id, treatment_remark, old_status, new_status) values (1, 3, 'Taking medication', 'UNDER_TREATMENT', 'CURED');
insert into patient_treatment_history (patient_id, doctor_id, treatment_remark, old_status, new_status) values (1, 3, 'Taking medication for 3 weeks', 'UNDER_TREATMENT', 'CURED');

insert into patient_medical_record_symptom (patient_medical_record_id, symptom_id) values (1, 2);
insert into patient_medical_record_symptom (patient_medical_record_id, symptom_id) values (1, 5);
insert into patient_medical_record_symptom (patient_medical_record_id, symptom_id) values (2, 1);
insert into patient_medical_record_symptom (patient_medical_record_id, symptom_id) values (2, 6);
insert into patient_medical_record_symptom (patient_medical_record_id, symptom_id) values (3, 3);
insert into patient_medical_record_symptom (patient_medical_record_id, symptom_id) values (3, 1);



