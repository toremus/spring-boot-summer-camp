package com.ag04smarts.sha.controllers;

import com.ag04smarts.sha.repositories.PatientRepository;
import com.ag04smarts.sha.services.PatientServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

class PatientControllerTest {

    private PatientController patientController;

    @Mock
    private PatientServiceImpl patientServiceNeurology;

    @Mock
    private PatientRepository patientRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        patientController = new PatientController(patientServiceNeurology, patientRepository);
    }

    @Test
    public void testMockMVC() throws Exception
    {
        MockMvc mockMvc = standaloneSetup(patientController).build();

        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/patient"))
                .andExpect(status().isOk());
    }

}