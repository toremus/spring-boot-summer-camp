package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.bootstrap.BootStrapData;
import com.ag04smarts.sha.domain.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class PatientRepositoryTestIT {

    @Autowired
    private PatientRepository patientRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void findByAgeAndEnlistmentDate() {

        List<Patient> patientList = patientRepository.findByAgeAndEnlistmentDate();

        assertTrue(patientList.size() == 2);

    }
}